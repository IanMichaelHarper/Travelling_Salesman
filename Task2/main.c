#include "header.h"

int main(int argc, char * argv[]){
	//option not to print details to the terminal
	int verbose = 0;
	char filename[256];
	if (verbose)
		fclose(stdout);
	int n, opt;
	int file_passed = false;
	if (argc < 3){
		printf("arg error\n");
		exit(1);
	}
	while ((opt = getopt(argc, argv,"n:f:")) != -1){
		switch (opt){
			case 'n': 
				n = atoi(optarg);
				break;
			case 'f': 
				strcpy(filename,optarg);
				file_passed = true;
				break;
		}
	}

	if (file_passed == true){
		city * cities;
		double ** distance;
		double * distance_rows;
		parse_file(filename, cities, distance, distance_rows, &n);
		print_cities(cities, n);	
		print_distance(distance, n);
	}
	else{
		city * cities = malloc(n*sizeof(city));
		double ** distance = calloc(n, sizeof(double));
		double * distance_rows = calloc(n*n, sizeof(double));

		//start with random int map
		int N = 20;
		int flag;
		int ** map = calloc(N, sizeof(int *));
		int * map_rows = calloc(N*N, sizeof(int));
		for (int i=0; i<N; i++){ //select n random points on map to place cities
			map[i] = &map_rows[i*N];
		}
		int * irand = calloc(n,sizeof(int));
		int * jrand = calloc(n,sizeof(int));
		srand(time(NULL));
		for (int i=0; i<n; i++){ //select n random points on map to place cities
			distance[i] = &distance_rows[i*n];
			cities[i].name = i;
			flag = 1;
			irand[i] = rand() % N;
			jrand[i] = rand() % N;
			//this section of code takes a very long time but need it when randomly picking cities - update: suddenly it doesnt?
			for (int j=0; j<i; j++){
				if (irand[i] == irand[j] && jrand[i] == jrand[j]){ //if random location is occupied by a city already
					flag = 0; // don't put city on map
					i--; //try again
					break;
				}	
			}
			if (flag){
				map[irand[i]][jrand[i]] = cities[i].name;
				cities[i].xloc = irand[i];
				cities[i].yloc = jrand[i];
			}
		}

		//printf("distance adjacency matrix: \n ------------------------------------------\n");	
		for (int i=0; i<n; i++){ 
			for (int j=i; j<n; j++){ //essentially calcuation half of an adjacency matrix so can start at i
	//			if (i == j) continue; //this will be 0 
				distance[i][j] = sqrt(pow(cities[i].xloc - cities[j].xloc, 2) +  pow(cities[i].yloc - cities[j].yloc, 2));
				distance[j][i] = distance[i][j];
			}
		//	for (int j=0; j<n; j++) //essentially calcuation half of an adjacency matrix so can start at i
				//printf("%lf ", distance[i][j]);
			//printf("\n");
		} 	

		//since different tours of the cities are essentially different permutations of a string of cities, we can just permute the cities array
		//eg. say our first tour is [0,1,2,3], then our second is [0,2,1,3], third is [0,3,2,1] etc.
		int * tour = malloc(n*sizeof(int));
		printf("starting tour: ");
		for (int i=0; i<n; i++){
			tour[i] = cities[i].name;		
			printf("%d ", tour[i]);
		}
		printf("\n");
		
		double shortest_tour = 100000;
		struct timeval start, end;
		long long time_taken;
		gettimeofday(&start, NULL);
		//for a fixed starting city, we just make the first arg of permute 1	
		get_shortest_tour(tour, 1, n-1, distance, &shortest_tour);
		gettimeofday(&end, NULL);
		time_taken = (end.tv_sec - start.tv_sec)*1000000L + (end.tv_usec - start.tv_usec);
		printf("shortest_tour = %lf\n", shortest_tour);
		printf("time taken = %lld microseconds\n", time_taken);

		FILE * f = fopen("brute_force.csv", "a");
		if (f == NULL){
			printf("Something went wrong, stop program");
			exit(1);
		}
		fprintf(f, "%d,%lld\n", n, time_taken);
		fclose(f);
		
		FILE * config = fopen("config.tsp", "w");
		if (config == NULL){
			printf("Something went wrong, stop program");
			exit(1);
		}
		fprintf(config, "NAME: Random configuration\n");
		fprintf(config, "TYPE: TSP\n");
		fprintf(config, "DIMENSION: %d\n", n);
		fprintf(config, "EDGE_WEIGHT_TYPE: EXPLICIT\n");
		fprintf(config, "EDGE_WEIGHT_FORMAT: FULL_MATRIX\n");
		fprintf(config, "DISPLAY_DATA_TYPE: TWOD_DISPLAY\n");
		fprintf(config, "EDGE_WEIGHT_SECTION\n");
		for (int i=0; i<n; i++){ 
			for (int j=0; j<n; j++)
				fprintf(config, "%lf ", distance[i][j]);
			fprintf(config, "\n");
		} 	
		fprintf(config, "NODE_COORD_SECTION\n");
		for (int i=0; i<n; i++){
			fprintf(config, "%d %d %d\n",cities[i].name,cities[i].xloc, cities[i].yloc);
		}
		fprintf(config, "EOF\n");
		fclose(f);
		
		free(tour);
		free(map_rows);
		free(map);
		free(distance_rows);
		free(distance);
		free(cities);
		free(irand);
		free(jrand);
	}
	return 0;
}
