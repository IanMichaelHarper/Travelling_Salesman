#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>
#include <string.h>

#define true 1
#define false 0

typedef struct City {
	int name; //just make name of city a number
	//location
	int xloc;
	int yloc;
}city;

void print_map(int ** map, int N);
void print_distance(double ** distance, int N);
void print_cities(city * cities, int n);
void swap(int *x, int *y);
void get_shortest_tour(int *a, int l, int r, double ** distance, double * shortest_tour);
double get_tour_length(int * tour, int n, double ** distance);
void parse_file(char * filename, city * cities, double ** distance, double * distance_rows, int * n);
