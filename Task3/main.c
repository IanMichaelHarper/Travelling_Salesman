#include "header.h"

int main(int argc, char * argv[]){
	//option not to print details to the terminal
	int verbose = 0;
	if (verbose)
		fclose(stdout);
	
	int n=29, opt; //just make default num cities 5
	char filename[256];
	city * cities;
	double ** distance;
	double * distance_rows;
	int file_passed = false;
	if (argc < 3){
		printf("arg error\n");
		exit(1);
	}
	//can only pass file or n, not both
	while ((opt = getopt(argc, argv,"n:f:")) != -1){
		switch (opt){
			case 'n': 
				n = atoi(optarg);
				break;
			case 'f': 
				strcpy(filename,optarg);
				file_passed = true;
				break;
		}
	}
	printf("3rd n = %d\n", n);
	
	cities = malloc(n*sizeof(city));
	distance = calloc(n, sizeof(double *));
	distance_rows = calloc(n*n, sizeof(double));
	if (file_passed == true){ //if true means no n passed
		printf("%s\n", filename);
		parse_file(filename, cities, distance, distance_rows, &n);
		print_distance(distance,n);
		print_cities(cities, n);
	//	free(distance_rows);
	//	free(distance);
	//	free(cities);
	}
	
	//start with int map
	int N = 20;
	int ** map = calloc(N, sizeof(int *));
	int * map_rows = calloc(N*N, sizeof(int));
	//int city, num_cities_placed = 0;
	int * irand = calloc(n,sizeof(int));
	int * jrand = calloc(n,sizeof(int));
	srand(time(NULL));
	for (int i=0; i<N; i++){
		map[i] = &map_rows[i*N];
		/*if (num_cities_placed < n){ //if not all cities on map yet
			for (int j=0; j<N; j++){
				//if current map location is one of our randomly generated points
				if (i == irand[num_cities_placed] && j == jrand[num_cities_placed]){ 
					map[i][j] = 1; //place the city on the map
					num_cities_placed++;
				}
			}
		}*/
	}
	for (int i=0; i<n; i++){ //select n random points on map to place cities
		distance[i] = &distance_rows[i*n];
		cities[i].name = i;
		int flag = 1;
		irand[i] = rand() % N;
		jrand[i] = rand() % N;
		//this section of code takes a very long time but need it when randomly picking cities - update: suddenly it doesnt?
		for (int j=0; j<i; j++){
			if (irand[i] == irand[j] && jrand[i] == jrand[j]){ //if random location is occupied by a city already
				flag = 0; // don't put city on map
				i--; //try again
				break;
			}	
		}
		if (flag){
			map[irand[i]][jrand[i]] = cities[i].name;
			cities[i].xloc = irand[i];
			cities[i].yloc = jrand[i];
		}
		//map[irand[i]][jrand[i]] = 1;
	}

	//printf("distance adjacency matrix: \n ------------------------------------------\n");	
	for (int i=0; i<n; i++){ 
		for (int j=i; j<n; j++){ //essentially calcuation half of an adjacency matrix so can start at i
//			if (i == j) continue; //this will be 0 
			distance[i][j] = sqrt(pow(cities[i].xloc - cities[j].xloc, 2) +  pow(cities[i].yloc - cities[j].yloc, 2));
			distance[j][i] = distance[i][j];
		}
	//	for (int j=0; j<n; j++) //essentially calcuation half of an adjacency matrix so can start at i
			//printf("%lf ", distance[i][j]);
		//printf("\n");
	} 	

	//since different tours of the cities are essentially different permutations of a string of cities, we can just permute the cities array
	//eg. say our first tour is [0,1,2,3], then our second is [0,2,1,3], third is [0,3,2,1] etc.
	if (file_passed == false){
		int * tour = malloc(n*sizeof(int));
		printf("starting tour: ");
		for (int i=0; i<n; i++){
			tour[i] = cities[i].name;		
			printf("%d ", tour[i]);
		}
		printf("\n");
		
		//for a fixed starting city, we just make the first arg of permute 1	
		double shortest_tour = 100000;
		struct timeval start, end;
		long long time_taken;
		gettimeofday(&start, NULL);
		get_shortest_tour(tour, 1, n-1, distance, &shortest_tour);
		gettimeofday(&end, NULL);
		time_taken = (end.tv_sec - start.tv_sec)*1000000L + (end.tv_usec - start.tv_usec);
		printf("shortest_tour = %lf\n", shortest_tour);
		printf("time taken = %lld microseconds\n", time_taken);

		FILE * file = fopen("brute_force.csv", "a");
		if (file == NULL) return -1;
		fprintf(file, "%d,%lld\n", n, time_taken);
		fclose(file);

		NN(tour, distance, n);
		printf("Nearest Neighbour tour: ");
		for (int i=0; i<n; i++){
			printf("%d ", tour[i]);
		}
		printf(" is of length %lf\n", get_tour_length(tour, n, distance));
		
		two_opt(tour, n, distance);	
		printf("2-opt tour tour for NN: ");
		for (int i=0; i<n; i++){
			printf("%d ", tour[i]);
		}
		printf(" is of length %lf\n", get_tour_length(tour, n, distance));

			
		for (int i=0; i<n; i++){
			tour[i] = i;
		}
		two_opt(tour, n, distance);	
		printf("2-opt tour tour for serial starting: ");
		for (int i=0; i<n; i++){
			printf("%d ", tour[i]);
		}
		printf(" is of length %lf\n", get_tour_length(tour, n, distance));
	
		free(tour);
		free(map_rows);
		free(map);
		free(distance_rows);
		free(distance);
		free(cities);
		free(irand);
		free(jrand);
	}
	return 0;
}
