#include "header.h"

void print_map(int ** map, int N){
	for (int i=0; i<N; i++){
		for (int j=0; j<N; j++){
			printf("%d ", map[i][j]);
		}
		printf("\n");
	}
}

void print_distance(double ** distance, int n){
	for (int i=0; i<n; i++){
		for (int j=0; j<n; j++){
			printf("%lf ", distance[i][j]);
		}
		printf("\n");
	}
}
void print_cities(city * cities, int n){
	printf("cities:\n");
	for (int j=0; j<n; j++){
		//printf("here");
		printf("%d  %d  %d\n", cities[j].name, cities[j].xloc, cities[j].yloc);
	}
}

/* Function to swap values at two pointers */
void swap(int *x, int *y){
    int temp;
    temp = *x;
    *x = *y;
    *y = temp;
}
 
/* Function to get shortest tour
 * first three parameters:
 *       1. int array
 *          2. Starting index of the array
 *             3. Ending index of the array. */
void get_shortest_tour(int *a, int l, int r, double ** distance, double * shortest_tour){
   int i;
   double tour_length;
   double tot_dist = 0;
   if (l == r){
	for (i = 0; i <= r; i++){
//	 printf("%d", a[i]);
	 //tot_dist += distance[l][i];
	 //printf("+%lf", distance[l][i]);
	}
	//printf("=%lf", tot_dist);
//	printf(", ");
	tour_length = get_tour_length(a, r, distance);
//	printf("tour length = %lf\n", tour_length);
	if (tour_length < *shortest_tour){
		*shortest_tour = tour_length; //race condition here if parallelized
	}
		//optional TODO: find a way to modify tour/return shortest tour - done in Task3 with other functions
//	printf("current shortest tour length = %lf\n", *shortest_tour);	
   }
   else{
       for (i = l; i <= r; i++){
//	  printf("swapping %d with %d\n", *(a+l), *(a+i));
          swap((a+l), (a+i));
          get_shortest_tour(a, l+1, r, distance, shortest_tour);
//	  printf("now swapping %d with %d\n", *(a+l), *(a+i));
          swap((a+l), (a+i)); //backtrack
       }
   }
}
double get_tour_length(int * tour, int n, double ** distance){
	double tot_dist = 0;
	int current_city, next_city, starting_city=tour[0];
	for (int i=0; i<n; i++){
		current_city = tour[i];
		next_city = tour[i+1];
		tot_dist += distance[current_city][next_city];
	}
	tot_dist += distance[next_city][starting_city];
	return tot_dist;
}

void get_num_cities_from_file(char * filename, int * n){
	size_t size=256, len;
	char* buffer = malloc(sizeof(char) * size);
	char dim[16] = "DIMENSION";
	int dim_count;
	char num_buf[8];
	int num_count = 1;
	char num_cities_string[8];
	char * p; //point to use to get dimension
	printf("%s\n", dim);
	for (int i=0; i<16; i++) 
		printf("%c ", dim[i]);
	printf("\n");
	FILE * f = fopen(filename,"r");
	if (f==NULL) exit(-1); 
	int j = 0;
	while (f!=NULL){
		dim_count = 0;
		len = getline(&buffer,&size,f);
		if (!feof(f)) {
//			printf ("Buffer size is %zu, buffer=<%s>\n",size,buffer);
			//test for dimension line
			if (dim_count != -1){
				for (int i=0; i<10; i++){
					if (buffer[i] != dim[i])
						break;
					else
						dim_count++;
				}
			
				//this section of code gets the number of cities
				//could prob do this outside for loop
				if (dim_count == 9 ){ //if buffer contains the dimension line oof tsp file
					p = strchr(buffer,'\n');  //get point to end of line
					//go backwards through dimension line until we hit the whitespace before the start of number of cities
					while (*(p-num_count) != ' '){ //all the files I donwloaded have a space between : and the dim number	
			//			printf("number = %c\n",*(p-num_count));
						num_count++; //go backwards to get number of digits in number of cities
					}
					num_count--; //get back to first nnumber rather than space
					while (*(p-num_count) != '\n'){ ///now go forwards again to get number of cities in order	
			//			printf("number = %c\n",*(p-num_count));
						num_cities_string[j] = *(p-num_count);  //store number of cities string
						num_count--;
						j++;
					}
			//		printf("%s\n", num_cities_string);
//					printf("%s\n", temp);
					*n = atoi(num_cities_string); //convert number of cities from string to int
					dim_count = -1; //ensure we dont test for dimension line again
					//allocate memory now that we know the number of cities we're dealing with
					//will only malloc once since dim_count is now -1
				}
			}
		}
		else {
			fclose(f);
			f=NULL;
		}
	}
}

void parse_file(char * filename, city * cities, double ** distance, double * distance_rows, int * n){
	printf("1st n = %d\n", *n);
	printf("%s\n", filename);
	size_t size=256, len;
	char* buffer = malloc(sizeof(char) * size);
	char dim[16] = "DIMENSION";
	int dim_count;
	char num_buf[8];
	int num_count = 1;
	char num_cities_string[8];
	char * p; //point to use to get dimension
	printf("%s\n", dim);
	for (int i=0; i<16; i++) 
		printf("%c ", dim[i]);
	printf("\n");
	FILE * f = fopen(filename,"r");
	if (f==NULL) exit(-1); 
	int j = 0;
	while (f!=NULL){
		dim_count = 0;
		len = getline(&buffer,&size,f);
		if (!feof(f)) {
//			printf ("Buffer size is %zu, buffer=<%s>\n",size,buffer);
			//test for dimension line
			if (strcmp(buffer, "DISPLAY_DATA_SECTION\n") == 0 || strcmp(buffer, "NODE_COORD_SECTION\n") == 0){
//				printf("getting coord data\n");
				//init counters	
				int row=0,col;
				int whitespace, digit = 0;
				float temp;	
				while (row < *n ){ //loop through rows
					whitespace = 10;
					col = 0;
					len = getline(&buffer,&size,f); 
//					printf("%s\n", buffer);
					for (int i=0; i<len; i++){ //loop through chars in line
						if (buffer[i] != ' '){ //distances are seperated by whitespace in file 
							whitespace = 0; //now we have a new column value
							num_buf[digit] = buffer[i]; //store individual digits
							digit++;
						}
						else if (buffer[i] == ' '){//reset after each whitespace
							digit = 0;
//							printf("%s\n", num_buf);
							if (whitespace == 0){ //if first time after reading a coord
								if (col == 0){
									temp = atof(num_buf);
									cities[row].name = (int)(temp); //assign
									col++; //new column
									printf("cities[row=%d].name = %d\n", row, cities[row].name);
								}
								//assigning to int might be a problem for tsp225.tsp since float
								//prob not now with casting to int
								else if (col == 1){
									temp = atof(num_buf);
									cities[row].xloc= (int)(temp); //assign
									col++; //new column
//									printf("cities[row=%d].xloc= %d\n", row, cities[row].xloc);
								}
								else if (col == 2){
									temp = atof(num_buf);
									cities[row].yloc = (int)(temp); //assign
									col++; //new column
//									printf("cities[row=%d].yloc= %d\n", row, cities[row].yloc);
								}
								memset(num_buf, 0, sizeof(num_buf)); //reset num_buf
							}
							whitespace++; //if double whitespace we wont assign a new col value
						}
					}
					row++;
				}
			}
			if (strcmp(buffer, "EDGE_WEIGHT_SECTION\n") == 0){ //init distance matrix from this section
				//init counters
				int row=0,col;
				int digit = 0, whitespace = 0;
				while (row < *n ){ //loop through rows
					col = 0;
					len = getline(&buffer,&size,f); 
					for (int i=0; i<len; i++){ //loop through chars in line
						if (buffer[i] != ' '){ //distances are seperated by whitespace in file 
							whitespace = 0; //now we have a new column value
							num_buf[digit] = buffer[i]; //store individual digits
							digit++;
						}
						else if (buffer[i] == ' '){//reset after each whitespace
							digit = 0;
							if (whitespace == 0){ //if first time after reading a distance
								distance[row][col] = atof(num_buf); //assign
								memset(num_buf, 0, sizeof(num_buf)); //reset num_buf
								col++; //new column
								//printf("%lf ",distance[row][col-1]);
							}
							whitespace++; //if double whitespace we wont assign a new col value
						}
					}
					//printf("\n");
					row++;
				}
			}
		}
		else {
			fclose(f);
			f=NULL;
		}
	}
	free(buffer);
	printf("2nd n = %d\n", *n);
}
#if 0
	city * cities = malloc(n*sizeof(city));
	double ** distance = calloc(n, sizeof(double));
	double * distance_rows = calloc(n*n, sizeof(double));

	//start with int map
	int N = 20;
	int ** map = calloc(N, sizeof(int *));
	int * map_rows = calloc(N*N, sizeof(int));
	int city, num_cities_placed = 0;
	int * irand = calloc(n,sizeof(int));
	int * jrand = calloc(n,sizeof(int));
	srand(time(NULL));
	for (int i=0; i<N; i++){
		map[i] = &map_rows[i*N];
		/*if (num_cities_placed < n){ //if not all cities on map yet
			for (int j=0; j<N; j++){
				//if current map location is one of our randomly generated points
				if (i == irand[num_cities_placed] && j == jrand[num_cities_placed]){ 
					map[i][j] = 1; //place the city on the map
					num_cities_placed++;
				}
			}
		}*/
	}

#endif
