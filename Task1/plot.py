import pandas as pd
#import matplotlib.pyplot as plt
import pylab as plt

filename = "brute_force.csv"
df = pd.read_csv(filename)

plt.figure()
plt.plot(df["Num Cities"], df["Time Taken"], 'o', linestyle='-')
plt.xlabel("Number of Cities n")
plt.ylabel("Time Taken (microseconds)")
plt.title("Time Taken to Solve TSP by Brute Force")
plt.savefig("brute_force.eps")
plt.show()
